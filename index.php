<!doctype hmtl>
<!--1.入力された文章を変数($text)へ代入する-->
<html>
<head>
  <meta charset="utf-8" />
  <title>感情解析</title>
  <style type="text/css">
    div.log{
      float: left;
      background-color: rgb(119, 186, 204);
      padding: 20px;
      margin: 20px;
    }
    div.debug{
      float: right;
      padding: 20px;
      margin: 20px;
      background-color: rgb(140, 212, 191);
    }
  </style>
</head>
<!--=====送信フォームここから=====-->
<body>
  <div id="form">
    <form class="a" method="post" action="index.php" >
      <input type="text" name="hoge"/>
      <input type="submit"  />
    </form>
  </div>
  <div>
  </div>
  <style>
    #form{
      background-color: rgb(128, 184, 57);
    }
  </style>
<!--=====送信フォームここまで=====-->
<?php
//=====$text受け取りここから=====
function get_Text(){
  echo '<div class="log">';
  echo '==実行開始 : getText()<br />';
  global $text;
  $text = $_POST['hoge'];
    echo '受け取った文字列 : '.$text.'<br />';

  echo '==実行終了 : getText()<br /></div><div class="debug">';
};
//=====$text受け取りここまで=====
//=====
function getFlow(){
    global $sentence_flow;
    global $text;
	$appid = "dj0zaiZpPWxRWUFsSjVBbzM4UCZzPWNvbnN1bWVyc2VjcmV0Jng9M2U-";
	//$text = "この鶏は僕が子供のころに拾ってきた鶏だ。";//ここに解析したい文章を入れる
	$url = "http://jlp.yahooapis.jp/DAService/V1/parse?appid=".$appid."&sentence=".urlencode($text);
	$xml = simplexml_load_file($url);
	$seg =null;//ここで初期化してあげる
	$chunks = $xml->Result->ChunkList->Chunk;
	foreach ($chunks as $chunk) {
		$id = (int)$chunk->Id;
		$dependency = (int)$chunk->Dependency;
		$morphems = $chunk->MorphemList->Morphem;
		foreach ($morphems as $morphem) {
			$seg = $seg.$morphem->Surface;
		}
		$sentence_flow[$id]['to'] = $dependency;//toに修飾するidを代入
		$str[$id][$dependency] = $seg;
		$seg = "";
	}
	for($i=0;$i<count($str);$i++){
		$key=array_keys($str[$i]);
		$sentence_flow[$i]['value'] =$str[$i][$key[0]];//valueに文字を代入
		$sentence_flow[$i]['score'] = 0;//scoreに0を代入
		echo $i."番<br>";//番号を表示
		echo $sentence_flow[$i]['value']."<br>";//valueの中身表示
		echo "to".$sentence_flow[$i]['to']."<br>";//toの中身表示
		echo "score".$sentence_flow[$i]['score']."<br>";//scoreの中身表示
		echo "<br>";//改行兄貴ｽｷｽｷ
	}
}
//=====
function getWord(){

  global $sentence_word;
  global $text;

  $sentence_word = array();
  $i = 0;

  $appid = 'dj0zaiZpPWxRWUFsSjVBbzM4UCZzPWNvbnN1bWVyc2VjcmV0Jng9M2U-';
  $url = 'http://jlp.yahooapis.jp/MAService/V1/parse?appid='.$appid.'&results=ma';
  $url .= "&sentence=".urlencode($text);

  $xml  = simplexml_load_file($url);

  foreach ($xml->ma_result->word_list->word as $value){
    $sentence_word[$i]['word'] = (string)$value->surface;
    $sentence_word[$i]['score'] = 0;
    $i++;
  }


  //echo $sentence_word[4]['word'];  //「し」が出力されます。
  //echo $sentence_word[2]['score']; //「0」が出力されます。
}
//=====
function getDic(){
  global $sentence_word;
  //$sentence_word = array('東京', '二行目', '名古屋');
  //$sentence_word = array();
   //$sentence_word[0]['word'] = '東京';
   //$sentence_word[0]['score'] = 10;
   //$sentence_word[1]['word'] = '二行目';
   //$sentence_word[1]['score'] = 20;
   //$sentence_word[2]['word'] = '名古屋';
   //$sentence_word[2]['score'] = 30;
  //  $sentence_word = array(
  //      array(
  //          'word' => 'うち',
  //          'score' => 0
  //          ),
  //      array(
  //          'word' => 'の',
  //          'score' => 0
  //          ),
  //      array(
  //          'word' => '庭',
  //          'score' => 0
  //          ),
  //      array(
  //          'word' => 'に',
  //          'score' => 0
  //          )
  //  );
  //read.textを$contentsに代入
  foreach ($sentence_word as $key => $word){
    for($i = 1;$i <= 6;$i++){
      $openFile = 'dic_'.$i.'.txt';
      $file = fopen($openFile, "r");//辞書ファイルオープン
      if($file){//辞書が取得できているか確認
        while ($line = fgets($file)) { //辞書から一行ずつ読み込み変数＄lineに代入

          //辞書検索ようの準備
          preg_match("/^([ぁ-んァ-ヶー一-龠]+)＠/u",$line,$preg);
          //echo $preg[1];
          $search_dic_word = $preg[1];
          //echo $search_dic_word.'<br />';

          //echo $line; *
          //$line = str_replace("\n", "<br>\n", $line);
          //$contentsの中に$sentece_wordがあった場合
          //if (strpos($line,$sentence_word[$key]['word']) !== false)//辞書で配列$sentence_wordの単語を検索
          if ($sentence_word[$key]['score'] == 0){
            //echo $search_dic_word;
            if ($sentence_word[$key]['word'] == $search_dic_word){
              //echo 'Hit!!';
              $mozime = strpos($line,'＠');
              preg_match("/＠([0-9]\.[0-9]+)/",$line,$retArr);
              $sentence_word[$key]['score'] = (float)$retArr[1];
              //echo '<br>';
              //echo $sentence_word[$key]['word']."==".$retArr[1]."<br />";
              //echo '<br>';
              break;
            }else{
            //echo 'NOHit!'; *
            //echo '<br>'; *
            }
          }
        }
      }else{
        echo '＜＜エラー＞＞辞書ファイルが開けませんでした。';
      }
    }
  }
  /* ファイルポインタをクローズ */
  fclose($file);
  //改行

  //echo '<pre>';
  //echo var_dump($sentence_word);
  //echo '</pre>';
}
//=====
function getResult(){
  global $sentence_flow;
  global $sentence_word;
  global $sentence_result;

  echo '<pre>';
  echo '$sentence_flow==';
  echo var_dump($sentence_flow);
  echo '</pre>';
  echo '<pre>';
  echo '$sentence_word==';
  echo var_dump($sentence_word);
  echo '</pre>';
//echo $sentence_word[0]['score'];

//配列の個数をカウント
  $m = count($sentence_flow) - 1;
  $n = count($sentence_word) - 1;
  //単語がどの文節に入っているのかを判断するところ
  $ii=0;
  $wordSum = 0;//文節ごとの単語スコアの合計用
  $wordCount = 0;

  for ($i=0;$i <= $m;$i++){//文節をループして取り出し$flowへ代入
    $flow = $sentence_flow[$i]['value'];

    for (;$ii <= $n;$ii++){
      $word = $sentence_word[$ii]['word'];
      echo '###'.$word.'###';
      if ($ii == $n){//全体の文章の最後の単語の時の処理
        $wordSum += $sentence_word[$ii]['score'];
        $wordCount++;
      }
      // if(strpos($flow,$word) === false || $ii == $n){//文節に単語が含まれているか判断
      //   //'abcd'のなかに'bc'が含まれていない場合
      //   $sentence_flow[$i]['score'] = $wordSum / $wordCount;
      //   $wordCount = 0;
      //   $wordSum = 0;
      //   break;
      // }
      echo '<br />'.$flow.'=!='.$word;
      $III = $i+1;
      echo '  //文節数='.$III.'  //単語数='.$ii;
      //echo '<br />$wordCount='.$wordCount.'      $wordSum='.$wordSum;
      //echo $word_score;
      //文節ごとに点数を平均して$sentence_flow[][score]に代入$wordCountはその文節の単語数
      $wordCount++;
      $wordSum += $sentence_word[$ii]['score'];

      if(strpos($flow,$word) === false || $ii == $n){//文節に単語が含まれているか判断
        //'abcd'のなかに'bc'が含まれていない場合
        $sentence_flow[$i]['score'] = $wordSum / $wordCount;
        $wordCount = 0;
        $wordSum = 0;
        $ii++;
        break;
      }

      echo '    $wordCount='.$wordCount.'      $wordSum='.$wordSum.'      $i='.$i.'<br />';
    }
    echo '<br />「'.$flow.'」の点数は'.$sentence_flow[$i]['score'].'<br /><br />';
  }


  echo '<pre>';
  var_dump($sentence_flow);
  echo '</pre>';
}
function getResult2(){
  global $sentence_flow;
  global $sentence_result;
  //アルゴリズムで各ステップで平均化
  foreach ($sentence_flow as $key => $value) {
    //echo $sentence_flow[$key]['value'];
    if($sentence_flow[$key]['to'] == -1){
        //echo $key;
      $keySave = $key;
      $flow_tree[0]['toNum'] = $sentence_flow[$key];
      break;
    }
  }
  #echo $keySave;//係り受ける最後の文節の引数$sentence_flow[$keySave]

  #echo '<pre>';
  #echo var_dump($flow_tree);
  #echo '</pre>';

  //第二階層以下は分岐しないものとすると。
  $flowSum = 0;
  $flowSumCount = 1;
  //$flowAve = 0;

  foreach ($sentence_flow as $key => $value) {
    echo '>>最後の文節に向かっている文節探し<br />';
    if($sentence_flow[$key]['to'] == $keySave){
      echo '>>みつかる。'.$key.'番の文節は最後の文節に向かっている<br />';
      echo '>>'.$key.'番めの文節の点数は'.$sentence_flow[$key]['score'].'<br />';
      $flowSum = $sentence_flow[$key]['score'];//A
      echo '>>最後の文節にくっっついている文節'.$key.'番目を合計計算用変数へ代入して'.$flowSum.'になったよ<br />';
      $search_2 = $key;//ヒットした文節番を抑える
      foreach ($sentence_flow as $key_2 => $value_2) {
          echo '>>>'.$search_2.'番めに向かっている文節を検索<br />';
        if($sentence_flow[$key_2]['to'] == $search_2){
          echo '>>>みつかる'.$key_2.'番めでした！<br />';
          $flowSumCount++;
          echo '>>>>その直線の単語数をカウントしてるよ、'.$flowSumCount.'個目だよ<br />';
          $flowSum += $sentence_flow[$key_2]['score'];
          echo 'この直線の点数の合計は'.$flowSum.'点（'.$flowSumCount.'個）<br />';
          break;
        }
      }
      $flowAve[$key] = (float)$flowSum / (float)$flowSumCount;
          echo '>>>>>平均を出すよ、この'.$key.'番目の行の合計点数は'.$flowSum.'点だよ。これを単語数'.$flowSumCount.'個で割るよだから平均は'.$flowAve.'点だよ。<br />';
      $flowSumCount = 1;
    }
  }

  #echo '<pre>';
  #echo var_dump($flowAve);
  #echo '<pre>';


  //結果を表示
  foreach ($flowAve as $key_3 => $value_3) {
    $sum += $value_3;
  }
  $sum = $sum / count($flowAve);
  $sum += $sentence_flow[$keySave]['score'];
  $sentence_result = $sum / 2;

  //echo '<font size=60px">この文章の算出スコアは'.$sentence_result.'</font>';
}
//=====
function showResult(){
  global $text;
  global $sentence_result;
  echo '<div class="result"><font size=60>';
  echo '「'.$text.'」の感情スコアは'.$sentence_result.'点です。';
  echo '</font></div>';
}
get_Text();
getFlow();
getWord();
getDic();
getResult();
getResult2();
showResult();
echo '</div>';//<div class=log>
echo '<pre>';
echo '＜クレジット＞
Hiroya Takamura, Takashi Inui, Manabu Okumura,
"Extracting Semantic Orientations of Words using Spin Model",
In Proceedings of the 43rd Annual Meeting of the Association for Computational Linguistics (ACL2005) ,
pages 133--140, 2005.
<!-- Begin Yahoo! JAPAN Web Services Attribution Snippet -->
<a href="http://developer.yahoo.co.jp/about">
<img src="http://i.yimg.jp/images/yjdn/yjdn_attbtn1_88_35.gif" width="88" height="35" title="Webサービス by Yahoo! JAPAN" alt="Web Services by Yahoo! JAPAN" border="0" style="margin:15px 15px 15px 15px"></a>
<!-- End Yahoo! JAPAN Web Services Attribution Snippet -->';
echo '</pre>';
